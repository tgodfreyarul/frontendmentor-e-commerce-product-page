// Cart popup functionality
const cartIcon = document.querySelector('.user-cart');
const cartDropdown = cartIcon.querySelector('.cart-dropdown');

cartIcon.addEventListener('click', () => {
  cartDropdown.classList.toggle('visible');
  // Toggle aria-hidden attribute for accessibility
  cartDropdown.setAttribute('aria-hidden', String(!cartDropdown.classList.contains('visible')));
});

// Close cart dropdown when clicking outside
document.addEventListener('click', function (event) {
  const isClickInsideCart = cartIcon.contains(event.target);
  const isClickInsideCartDropdown = cartDropdown.contains(event.target);
  if (!isClickInsideCart && !isClickInsideCartDropdown && cartDropdown.classList.contains('visible')) {
    cartDropdown.classList.remove('visible');
    cartDropdown.setAttribute('aria-hidden', 'true');
  }
});

// Prevent cart dropdown from closing when clicking inside it
cartDropdown.addEventListener('click', function (event) {
  event.stopPropagation();
});

// Données du produit
const productData = {
  name: 'Fall Limited Edition Sneakers',
  price: 125.00,
  imageUrl: 'images/image-product-1-thumbnail.jpg'
};

// Éléments du DOM
const cartContent = document.querySelector('.cart-content');
const cartCounter = document.querySelector('.cart-counter');
const quantityDisplay = document.querySelector('.quantity');
let totalQuantityInCart = 0;

// Mettre à jour le compteur du panier
const updateCartCounter = (quantity) => {
  cartCounter.textContent = quantity;
  cartCounter.style.display = quantity > 0 ? 'block' : 'none';
};

// Créer l'HTML d'un article du panier
const createCartItemHtml = (product, quantity) => {
  const totalPrice = (product.price * quantity).toFixed(2);
  return `
    <div class="cart-item">
      <img src="${product.imageUrl}" alt="${product.name}" class="cart-item-image" />
      <div class="cart-item-details">
        <p class="cart-item-title">${product.name}</p>
        <p class="cart-item-price">$${product.price} x ${quantity} <span>$${totalPrice}</span></p>
      </div>
      <button class="cart-item-remove" onclick="clearCart()">
        <img src="images/icon-delete.svg" alt="Remove item" />
      </button>
    </div>`;
};

// Mettre à jour le panier
const updateCart = (product, quantity) => {
  cartContent.innerHTML = quantity > 0 ?
    createCartItemHtml(product, quantity) + '<button class="checkout-button">Checkout</button>' :
    '<p class="empty-cart-msg">Your cart is empty.</p>';
  updateCartCounter(quantity);
};

// Gérer les clics sur les boutons de quantité
const handleQuantityChange = (change) => {
  let currentQuantity = parseInt(quantityDisplay.textContent, 10);
  let newQuantity = Math.min(Math.max(currentQuantity + change, 0), 12 - totalQuantityInCart);
  quantityDisplay.textContent = newQuantity.toString();
};

// Initialiser ou vider le panier
const clearCart = () => {
  totalQuantityInCart = 0;
  updateCart(productData, totalQuantityInCart);
};

// Ajouter au panier
document.querySelector('.add-to-cart-button-grp3').addEventListener('click', () => {
  let quantityToAdd = parseInt(quantityDisplay.textContent, 10);
  if (quantityToAdd > 0) {
    totalQuantityInCart += quantityToAdd;
    updateCart(productData, totalQuantityInCart);
    handleQuantityChange(-quantityToAdd);
  }
});

// Écouteurs d'événements pour les boutons d'augmentation/diminution
document.querySelector('.decrease-quantity').addEventListener('click', () => handleQuantityChange(-1));
document.querySelector('.increase-quantity').addEventListener('click', () => handleQuantityChange(1));

// Initialiser le panier comme vide
clearCart();

document.querySelectorAll('.thumbnail-button').forEach(thumbnail => {
  thumbnail.addEventListener('click', function () {
    // Update main product image
    const mainImage = document.querySelector('.main-product-image-rectangle');
    const newSrc = this.querySelector('.thumbnail-rectangle').src.replace('-thumbnail', '');
    mainImage.src = newSrc;

    // Remove active class from all thumbnails
    document.querySelectorAll('.thumbnail-button.active').forEach(active => {
      active.classList.remove('active');
    });

    // Add active class to clicked thumbnail
    this.classList.add('active');
  });

});

// Attach thumbnail event listeners to dynamically created galleries
function attachThumbnailEventListeners(clone) {
  const thumbnailButtons = clone.querySelectorAll('.thumbnail-button');
  thumbnailButtons.forEach((button, index) => {
    button.addEventListener('click', () => {
      currentIndex = index;
      updateCarousel(clone, currentIndex);
    });
  });
}

// Attachez les événements à la galerie originale
attachThumbnailEventListeners(document);

// JavaScript pour ouvrir le modal
const modal = document.getElementById('productModal');
const originalGallery = document.getElementById('originalGallery');
const modalGalleryContent = document.getElementById('modalGalleryContent');
const closeButton = document.querySelector('.close-button');
const mainImage = document.getElementById('mainImage');

let currentIndex = 0;

mainImage.addEventListener('click', () => {
  modalGalleryContent.innerHTML = '';
  const clone = originalGallery.cloneNode(true);
  clone.removeAttribute('id');
  modalGalleryContent.appendChild(clone);
  attachThumbnailEventListeners(clone);
  modal.style.display = 'flex';

  const closeButtonClone = closeButton.cloneNode(true);
  closeButtonClone.addEventListener('click', () => {
    modal.style.display = 'none';
  });
  clone.prepend(closeButtonClone);

  setupCarousel(clone);
});

function setupCarousel(clone) {
  const arrowContainer = setupCarouselNavigation(clone);
  clone.prepend(arrowContainer);
  updateCarousel(clone, currentIndex);
}

function setupCarouselNavigation(clone) {
  const arrowContainer = document.createElement('div');
  arrowContainer.className = 'modal-arrow-container';

  const navigateCarousel = (direction) => {
    const length = clone.querySelectorAll('.thumbnail-rectangle').length;
    currentIndex = (currentIndex + (direction === 'previous' ? -1 : 1) + length) % length;
    updateCarousel(clone, currentIndex);
  };

  const leftArrowButton = createArrowButton('previous', () => navigateCarousel('previous'));
  const rightArrowButton = createArrowButton('next', () => navigateCarousel('next'));

  arrowContainer.append(leftArrowButton, rightArrowButton);
  return arrowContainer;
}

function updateCarousel(clone, index) {
  const thumbnails = clone.querySelectorAll('.thumbnail-rectangle');
  const newSrc = thumbnails[index].src.replace('-thumbnail', '');
  clone.querySelector('.main-product-image-rectangle').src = newSrc;

  clone.querySelectorAll('.thumbnail-button').forEach((btn, i) => {
    btn.classList.toggle('active', i === index);
  });
}

function createArrowButton(direction, onClick) {
  const button = document.createElement('button');
  button.className = `modal-arrow modal-arrow-${direction}`;
  button.innerHTML = `<img src="images/icon-${direction}.svg" alt="${direction}">`;
  button.addEventListener('click', onClick);
  return button;
}

// Fermeture du modal
closeButton.addEventListener('click', () => {
  console.log("Fermeture du modal");
  modal.style.display = 'none';
});
// Fermeture du modal en cliquant en dehors
window.addEventListener('click', (event) => {
  if (event.target === modal) {
    console.log("Clic en dehors du modal, fermeture du modal");
    modal.style.display = 'none';
  }
});

const breakpointMobile = 375;

// This function creates a button for navigating the carousel
function createMobileArrowButton(direction, onClick) {
  const button = document.createElement('button');
  button.classList.add('mobile-arrow', `mobile-arrow-${direction}`);
  button.innerHTML = `<img src="images/icon-${direction}.svg" alt="${direction} arrow">`;
  button.addEventListener('click', onClick);
  return button;
}

// This function adds the navigation arrows to the carousel if it's in a mobile view
function addMobileArrows() {
  if (window.innerWidth <= breakpointMobile) {
    const gallery = document.querySelector('.product-gallery-grp10');
    // Check if arrows already exist to avoid duplicates
    const existingArrows = gallery.querySelectorAll('.mobile-arrow');
    if (existingArrows.length === 0) {
      const leftArrow = createMobileArrowButton('previous', () => navigateCarousel('previous'));
      const rightArrow = createMobileArrowButton('next', () => navigateCarousel('next'));
      gallery.insertBefore(leftArrow, gallery.firstChild);
      gallery.appendChild(rightArrow);
    }
  } else {
    // If not in mobile view, remove the arrows if they exist
    const existingArrows = document.querySelectorAll('.mobile-arrow');
    existingArrows.forEach(arrow => arrow.remove());
  }
}

// Attach the function to the window resize event
window.addEventListener('resize', addMobileArrows);

// Call the function when the script loads
addMobileArrows();

function navigateCarousel(direction) {
  const gallery = document.querySelector('.product-gallery-grp10');
  const images = gallery.querySelectorAll('.thumbnail-rectangle');
  const totalImages = images.length;

  if (direction === 'next') {
    currentIndex = (currentIndex + 1) % totalImages;
  } else if (direction === 'previous') {
    currentIndex = (currentIndex - 1 + totalImages) % totalImages;
  }

  const newSrc = images[currentIndex].src.replace('-thumbnail', '');
  document.querySelector('.main-product-image-rectangle').src = newSrc;

  // Update active class on thumbnails if needed
  document.querySelectorAll('.thumbnail-button').forEach((button, index) => {
    button.classList.toggle('active', index === currentIndex);
  });
}

// This function adds the navigation arrows to the carousel if it's in a mobile view
function updateMobileArrows() {
  const gallery = document.querySelector('.product-gallery-grp10');
  const existingArrows = gallery.querySelectorAll('.mobile-arrow');

  if (window.innerWidth <= breakpointMobile && existingArrows.length === 0) {
    const leftArrow = createMobileArrowButton('previous', () => navigateCarousel('previous'));
    const rightArrow = createMobileArrowButton('next', () => navigateCarousel('next'));
    gallery.insertBefore(leftArrow, gallery.firstChild);
    gallery.appendChild(rightArrow);
  } else if (window.innerWidth > breakpointMobile) {
    existingArrows.forEach(arrow => arrow.remove());
  }
}
// Debounce function to limit the rate at which a function can fire.
function debounce(func, wait, immediate) {
  let timeout;
  return function () {
    const context = this,
      args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

// Initialize and update arrows on load and resize with debounce
const debouncedAddMobileArrows = debounce(updateMobileArrows, 250);
window.addEventListener('DOMContentLoaded', debouncedAddMobileArrows);
window.addEventListener('resize', debouncedAddMobileArrows);




// Gérer l'ouverture et la fermeture du menu hamburger
const hamburger = document.querySelector('.menu-icon-button');
const navList = document.querySelector('.nav-list-grp12');
const navLinks = navList.querySelectorAll('.nav-link');

hamburger.addEventListener('click', function () {
  // Toggle the 'active' class on the hamburger and the nav list
  const isActive = this.classList.toggle('active');
  navList.classList.toggle('active');

  // Update the 'aria-expanded' attribute
  this.setAttribute('aria-expanded', isActive);
});

// Event handler for each link in the nav list
navLinks.forEach(link => {
  link.addEventListener('click', function () {
    // Close the nav list by removing the 'active' class
    navList.classList.remove('active');
    hamburger.classList.remove('active');
    hamburger.setAttribute('aria-expanded', 'false');
  });
});

// Select the close button
const closeNavButton = document.querySelector('.close-nav');

// Close the nav list when the close button is clicked
closeNavButton.addEventListener('click', function () {
  navList.classList.remove('active');
  hamburger.classList.remove('active');
  hamburger.setAttribute('aria-expanded', 'false');
});